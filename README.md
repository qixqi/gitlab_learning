# How to use gitlab

# 一、注册以及基本的使用

[点击此处，查看说明文档](http://210.30.96.133:8888/OpenSource_19/gitlab_learning/blob/master/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.pdf)

# 二、常用命令

![常用命令](./order.png)

# 大连理工大学-软件优化组

[实验室主页](http://oscar-lab.org/chn/introduction.htm)

&nbsp;&nbsp;大连理工大学软件优化组面向网络开放环境下大规模软件系统的主要发展趋势和工程需求，重点关注基于搜索的软件工程和软件仓库挖掘。针对关键技术和瓶颈问题开展系统研究，形成了以搜索空间变换为核心方法、以开源软件系统为典型应用的研究思路。相关结果已经在包括IEEE TSE, TKDE, TSMCB, TCYB, 中国科学、科学通报、计算机学报及ICSE, GECCO, PPSN, SEKE等上发表。
顶端。

&nbsp;&nbsp;对实验室感兴趣的同学可以加入我们，发送邮件至任老师邮箱**zren@dlut.edu.cn**了解详情。

（如果你是个害羞的孩子，也可以发到**Drogon.fan@gmail.com**，可以先和师哥师姐了解情况哦！）

&nbsp;&nbsp;●ω●我们在这里等着你哦!●ω●